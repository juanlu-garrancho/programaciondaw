package programaciondaw;

import java.util.Scanner;

public class EdadActual {

	public static void main(String[] args) {
		
		int nacimiento, actual, edad;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Introduzca su año de nacimiento:");
		nacimiento = sc.nextInt();
		System.out.println("Introduzca el año actual:");
		actual = sc.nextInt();
		sc.close();
		edad = actual - nacimiento;
		System.out.println("Tienes " + edad + " años de edad.");
	}

}
