package programaciondaw;

public class RangoShort {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Vamos a comprobar que los tipos se comportan de forma cíclica sumando 1
		// a su valor máximo 32676.
		short maximo = 32767;
		System.out.println("El valor mínimo del tipo short es " + (++ maximo));
	}

}
