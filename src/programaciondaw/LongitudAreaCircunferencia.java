package programaciondaw;

import java.util.Locale;
import java.util.Scanner;

public class LongitudAreaCircunferencia {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double radio, longitud, area;
		Scanner sc = new Scanner(System.in);
		sc.useLocale(Locale.US);
		
		System.out.println("Introduzca el radio de la circunferencia");
		radio = sc.nextDouble();
		sc.close();
		longitud = 2 * Math.PI * radio;
		area = Math.PI * Math.pow(radio, 2);
		System.out.println("La longitud de la circunferencia es: " + longitud);
		System.out.println("El área de la circunferencia es: " + area);
	}

}
