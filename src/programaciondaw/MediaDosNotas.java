package programaciondaw;

import java.util.Locale;
import java.util.Scanner;

public class MediaDosNotas {

	public static void main(String[] args) {
		// creamos las variables y creamos un objeto de la clase Scanner para
		// leer la entrada por teclado
		double nota_1, nota_2, media;
		Scanner sc = new Scanner(System.in);
		sc.useLocale(Locale.US); // Permite introducir los decimales con "."
		// Solicitamos, introducimos y hacemos la media de las notas.
		// Lo presentamos por consola
		System.out.println("Introduzca la primera nota:");
		nota_1 = sc.nextDouble();
		System.out.println("Introduzca la segunda nota: ");
		nota_2 = sc.nextDouble();
		sc.close();
		media = (nota_1 + nota_2) / 2.0;
		System.out.println("Su nota media es un " + media);
	}

}
