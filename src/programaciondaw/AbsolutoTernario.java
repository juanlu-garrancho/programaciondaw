package programaciondaw;

import java.util.Scanner;

public class AbsolutoTernario {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int numero;
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Introduzca un número: ");
		numero = sc.nextInt();
		sc.close();
		
		numero = numero < 0 ? -1 * numero : numero;
		System.out.println("Su valor absoluto es: " + numero);
	}

}
