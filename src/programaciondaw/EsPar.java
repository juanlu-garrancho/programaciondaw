package programaciondaw;

import java.util.Scanner;

public class EsPar {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int par;
		boolean comparacion;
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Introduce un número:");
		par = sc.nextInt();
		sc.close();
		comparacion = par % 2 == 0;
		System.out.println("Es par: " + comparacion);
	}

}
