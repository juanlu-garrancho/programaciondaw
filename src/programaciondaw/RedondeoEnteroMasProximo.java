package programaciondaw;

import java.util.Locale;
import java.util.Scanner;

public class RedondeoEnteroMasProximo {

	public static void main(String[] args) {
		double numero;
		int intMasProximo;
		Scanner sc = new Scanner(System.in);
		sc.useLocale(Locale.US);
		
		System.out.print("Introduzca un número: ");
		numero = sc.nextDouble();
		sc.close();
		intMasProximo = (int)(numero + 0.5);
		System.out.println("El número entero más proximo es: " + intMasProximo);

	}

}
