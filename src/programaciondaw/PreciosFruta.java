package programaciondaw;

import java.util.Scanner;
import java.util.Locale;

public class PreciosFruta {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double perasSemestre1, perasSemestre2, manzanasSemestre1, manzanasSemestre2, total;
		final double precioPeras = 1.95;
		final double precioManzanas = 2.35;
		Scanner sc = new Scanner(System.in);
		sc.useLocale(Locale.US);
		
		
		System.out.println("Para las peras");
		System.out.print("Introduzca los kg vendidos el primer semestre: ");
		perasSemestre1 = sc.nextDouble();
		System.out.print("Introduzca los kg vendidos el segundo semestre: ");
		perasSemestre2 = sc.nextDouble();
		
		System.out.println("Para las manzanas");
		System.out.print("Introduzca los kg vendidos el primer semestre: ");
		manzanasSemestre1 = sc.nextDouble();
		System.out.print("Introduzca los kg vendidos el segundo semestre: ");
		manzanasSemestre2 = sc.nextDouble();
		sc.close();
		
		total = (perasSemestre1 + perasSemestre2) * precioPeras;
		total += (manzanasSemestre1 + manzanasSemestre2) * precioManzanas;
		System.out.println("Los ingresos totales son " + total + "€");
	}

}
