package programaciondaw;

import java.util.Scanner;

public class SalirCalle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		boolean lluvia, tarea, biblioteca, salir;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("¿Llueve?:");
		lluvia = sc.nextBoolean();
		System.out.println("¿Has terminado tus tareas?:");
		tarea = sc.nextBoolean();
		System.out.println("Tienes que ir a la biblioteca");
		biblioteca = sc.nextBoolean();
		sc.close();
		
		salir = (!lluvia && tarea) || biblioteca;
		System.out.println("¿Puedes salir?: " + salir);
		
		
	}

}
