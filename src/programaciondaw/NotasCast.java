package programaciondaw;

import java.util.Locale;
import java.util.Scanner;

public class NotasCast {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double trimestre1, trimestre2, trimestre3, notaExpediente;
		int notaBoletin;
		Scanner sc = new Scanner(System.in);
		sc.useLocale(Locale.US);
		System.out.print("Introduzca la nota del primer trimestre: ");
		trimestre1 = sc.nextDouble();
		System.out.print("Introduzca la nota del segundo trimestre: ");
		trimestre2 = sc.nextDouble();
		System.out.print("Introduzca la nota del tercer trimestre: ");
		trimestre3 = sc.nextDouble();
		sc.close();
		
		notaExpediente = (trimestre1 + trimestre2 + trimestre3) / 3;
		notaBoletin = (int)notaExpediente;
		
		System.out.println("Nota boletín: " + notaBoletin);
		System.out.println("Nota expediente: " + notaExpediente);
	}

}
