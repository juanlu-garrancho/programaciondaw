package programaciondaw;

import java.util.Scanner;

public class EdadMasUno {

	public static void main(String[] args) {
		int edad;
		Scanner sc = new Scanner(System.in);
		System.out.println("Introduzca su edad");
		edad = sc.nextInt();
		sc.close();
		System.out.println("El año que viene tendrás " + (++ edad) + " años.");
	}

}
