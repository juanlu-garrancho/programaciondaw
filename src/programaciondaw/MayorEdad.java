package programaciondaw;

import java.util.Scanner;

public class MayorEdad {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int edad;
		boolean comparacion;
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Introduce tu edad:");
		edad = sc.nextInt();
		sc.close();
		comparacion = edad >= 18;
		System.out.println("Eres mayor de edad: " + comparacion);
	}

}
