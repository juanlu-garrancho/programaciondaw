package programaciondaw;

import java.util.Locale;
import java.util.Scanner;

public class IntroduccionScanner {

	public static void main(String[] args) {
		// Creamos variable numero e instanciamos un objeto de la clase Scanner
		double numero;
		Scanner sc = new Scanner(System.in);
		sc.useLocale(Locale.US); //Nos permite introducir "." como decimal
		// Pedimos e introducimos numero por consola
		System.out.println("Introduzca un número");
		numero = sc.nextDouble();
		System.out.println("El número introducido es el " + numero);
		sc.close();
	}

}
